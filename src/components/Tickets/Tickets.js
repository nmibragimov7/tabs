import React from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

const Tickets = (props) => {

    return (
        <>
            <Grid container direction="row" spacing={2}>                
                <Grid item>
                    Ticket id
                </Grid>
                <Grid item>
                    Ticket name
                </Grid> 
                <Grid item>
                    Ticket description
                </Grid>  
                <Grid item>
                    <Button type="submit" color="primary" variant="contained" onClick={props.handleChange}>Select</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default Tickets;